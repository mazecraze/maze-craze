#include <Adafruit_NeoPixel.h>

#define LED_STRIPS 12
#define powerPin 22

  Adafruit_NeoPixel leds [LED_STRIPS];
  
  enum Colors{
    RED, //0
    GREEN, //1
    BLUE, //2   
    WHITE, //3
    BRIGHTPINK, //4
    ORANGE, // 5
    BROWN, // 6
    BLACK, //7
  };




  Colors horCannon [6][5];// Current Maze
  int horR [6][5]; // row, col
  int horG  [6][5];
  int horB [6][5];
  Colors vertCannon [5][6]; //Current Maze
  int vertR [5][6]; // row, col
  int vertG [5][6];
  int vertB [5][6];

  Colors hor1 [6][5];
  Colors hor2 [6][5];
  Colors vert1[5][6];
  Colors vert2[5][6];

  

  

  Adafruit_NeoPixel leds2 = Adafruit_NeoPixel (15, 2, NEO_GRB + NEO_KHZ800);
  Adafruit_NeoPixel leds3 = Adafruit_NeoPixel (15, 3, NEO_GRB + NEO_KHZ800);
  Adafruit_NeoPixel leds4 = Adafruit_NeoPixel (15, 4, NEO_GRB + NEO_KHZ800);
  Adafruit_NeoPixel leds5 = Adafruit_NeoPixel (15, 5, NEO_GRB + NEO_KHZ800);
  Adafruit_NeoPixel leds6 = Adafruit_NeoPixel (15, 6, NEO_GRB + NEO_KHZ800);
  Adafruit_NeoPixel leds7 = Adafruit_NeoPixel (15, 7, NEO_GRB + NEO_KHZ800);
  Adafruit_NeoPixel leds8 = Adafruit_NeoPixel (15, 8, NEO_GRB + NEO_KHZ800);
  Adafruit_NeoPixel leds9 = Adafruit_NeoPixel (15, 9, NEO_GRB + NEO_KHZ800);
  Adafruit_NeoPixel leds10 = Adafruit_NeoPixel (15, 10, NEO_GRB + NEO_KHZ800);
  Adafruit_NeoPixel leds11 = Adafruit_NeoPixel (15, 11, NEO_GRB + NEO_KHZ800);
  Adafruit_NeoPixel leds12 = Adafruit_NeoPixel (15, 12, NEO_GRB + NEO_KHZ800);
  Adafruit_NeoPixel leds13 = Adafruit_NeoPixel (15, 13, NEO_GRB + NEO_KHZ800);
 
void setup(){
  // LED Setup
  leds2.begin();
  leds3.begin();
  leds4.begin();
  leds5.begin();
  leds6.begin();
  leds7.begin();
  leds8.begin();
  leds9.begin();
  leds10.begin();
  leds11.begin();
  leds12.begin();
  leds13.begin(); 
   
  randomSeed(analogRead(0));

  // Hall Effect Setup
  pinMode(powerPin, OUTPUT);
  pinMode(23, INPUT);  
 generateNewMaze();
 Serial.begin(9600);

}

int currentMaze = 1;
boolean playing = true;


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


void loop(){
  for(int i = 0; i < 6; i++){
    for(int j = 0; j < 5; j++){
      horCannon [i][j] = hor1[i][j];
    }
  }
  for(int i = 0; i < 6; i++){
    for(int j = 0; j < 5; j++){
      vertCannon [i][j] = vert1[i][j];
    }
  }

  displayArrays();

  delay(2000);
  for(int i = 0; i < 6; i++){
    for(int j = 0; j < 5; j++){
      horCannon [i][j] = hor2[i][j];
    }
  }
  for(int i = 0; i < 6; i++){
    for(int j = 0; j < 5; j++){
      vertCannon [i][j] = vert2[i][j];
    }
  }

  displayArrays();

  delay(2000);
 // leds2.setPixelColor(2, 30, 0, 0);

}



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// Converts enum "Colors" horCannon and vertCannon into RGB values
void cannonToRGB(){
  // horizontal
  for(int i = 0; i<6; i++){
    for(int j = 0; j<5; j++){
      switch(horCannon[i][j]){
        case RED:
          horR[i][j] = 30;
          horG[i][j] = 0;
          horB[i][j] = 0;
          break;

        case BLUE:
          horR[i][j] = 0;
          horG[i][j] = 0;
          horB[i][j] = 40;
          break;

        case GREEN:
          horR[i][j] = 0;
          horG[i][j] = 30;
          horB[i][j] = 0;
          break; 

        case WHITE:
          horR[i][j] = 40;
          horG[i][j] = 40;
          horB[i][j] = 40;
          break;

        case BLACK:
          horR[i][j] = 0;
          horG[i][j] = 0;
          horB[i][j] = 0;
          break;
          
        case BRIGHTPINK:
          horR[i][j] = 40;
          horG[i][j] = 16;
          horB[i][j] = 16;
          break;

        case ORANGE:
          horR[i][j] = 40;
          horG[i][j] = 20;
          horB[i][j] = 0;
          break;

        case BROWN:
          horR[i][j] = 4;
          horG[i][j] = 2;
          horB[i][j] = 1;
          break;

        default:
          horR[i][j] = 0;
          horG[i][j] = 0;
          horB[i][j] = 0;
          break;
        
      }
    }
  }

  // vertical
  for(int i = 0; i<5; i++){
    for(int j = 0; j<6; j++){
      switch(vertCannon[i][j]){
        case RED:
          vertR[i][j] = 30;
          vertG[i][j] = 0;
          vertB[i][j] = 0;
          break;

        case BLUE:
          vertR[i][j] = 0;
          vertG[i][j] = 0;
          vertB[i][j] = 40;
          break;

        case GREEN:
          vertR[i][j] = 0;
          vertG[i][j] = 30;
          vertB[i][j] = 0;
          break; 

        case WHITE:
          vertR[i][j] = 40;
          vertG[i][j] = 40;
          vertB[i][j] = 40;
          break;

        case BLACK:
          vertR[i][j] = 0;
          vertG[i][j] = 0;
          vertB[i][j] = 0;
          break;

        case BRIGHTPINK:
          vertR[i][j] = 40;
          vertG[i][j] = 16;
          vertB[i][j] = 16;
          break;
          
        case ORANGE:
          vertR[i][j] = 40;
          vertG[i][j] = 20;
          vertB[i][j] = 0;
          break;

       case BROWN:
          vertR[i][j] = 83;
          vertG[i][j] = 21;
          vertB[i][j] = 21;
          break;


          
        default:
          vertR[i][j] = 0;
          vertG[i][j] = 0;
          vertB[i][j] = 0;
          break;
      }
    }
  }
}

// Shows all LEDS after sending color data
void showAll(){
  leds2.show();
  leds3.show();
  leds4.show();
  leds5.show();
  leds6.show();
  leds7.show();
  leds8.show();
  leds9.show();
  leds10.show();
  leds11.show();
  leds12.show();
  leds13.show();
}

// Takes hor[][] and vert[][] and converts to LED info
void displayArrays(){
  cannonToRGB();
  // hor[][] processing
  for(int col = 0; col < 5; col++){
    for(int i = 0; i < 3; i ++){
       leds2.setPixelColor(col*3+i, horR[0][col], horG[0][col], horB[0][col]);
    }  
    for(int i = 0; i < 3; i ++){
       leds3.setPixelColor(col*3+i, horR[1][col], horG[1][col], horB[1][col]);
    }
    for(int i = 0; i < 3; i ++){
       leds4.setPixelColor(col*3+i, horR[2][col], horG[2][col], horB[2][col]);
    }
    for(int i = 0; i < 3; i ++){
       leds5.setPixelColor(col*3+i, horR[3][col], horG[3][col], horB[3][col]);
    }
    for(int i = 0; i < 3; i ++){
       leds6.setPixelColor(col*3+i, horR[4][col], horG[4][col], horB[4][col]);
    }
    for(int i = 0; i < 3; i ++){
       leds7.setPixelColor(col*3+i, horR[5][col], horG[5][col], horB[5][col]);
    }       
  }
  // vert[][] processing

  for(int row = 0; row <5; row++){
    for(int i = 0; i <3; i++){
        leds8.setPixelColor(row*3+i, vertR[row][0], vertG[row][0], vertB[row][0]);
    }
    for(int i = 0; i <3; i++){
        leds9.setPixelColor(row*3+i, vertR[row][1], vertG[row][1], vertB[row][1]);
    }
    for(int i = 0; i <3; i++){
        leds10.setPixelColor(row*3+i, vertR[row][2], vertG[row][2], vertB[row][2]);
    }
    for(int i = 0; i <3; i++){
        leds11.setPixelColor(row*3+i, vertR[row][3], vertG[row][3], vertB[row][3]);
    }
    for(int i = 0; i <3; i++){
        leds12.setPixelColor(row*3+i, vertR[row][4], vertG[row][4], vertB[row][4]);
    }
    for(int i = 0; i <3; i++){
        leds13.setPixelColor(row*3+i, vertR[row][5], vertG[row][5], vertB[row][5]);
    }
    
  }
  
  showAll();
}


void generateNewMaze(){
  int maze[5][5];
    for(int r = 0; r < 5; r++){
      for(int c = 0; c < 5; c++){
        int n = (int)(random(0,2));
        int s = (int)(random(0,2));
        int e = (int)(random(0,2));
        int w = (int)(random(0,2));
        maze[r][c] = (1000*n)+(100*s)+(10*e)+w;
      }
    }
    for(int i = 0; i < 5; i++){
      if(maze[0][i]>=1000)
        maze[0][i] = maze[0][i]-1000;
    }
    for(int i = 0; i < 5; i++){
      if((maze[4][i]/100)%2==1){
        maze[4][i] = maze[4][i]-100;
      }
    }
    for(int i = 0; i < 5; i++){
      if(maze[i][0]%2==1){
        maze[i][0] = maze[i][0]-1;
      }
    }
    for(int i = 0; i < 5; i++){
      if((maze[i][4]/10)%2==1){
        maze[i][4] = maze[i][4]-10;
      }
    }

    //Solution Implementer
    int startRow = 0; //actually the ending point
    int startCol = 0;
    int currentRow = startRow;
    int currentCol = startCol;
    int numMoves = 0;
    while((currentRow!=4)||(currentCol!=4)){  
      int rowChange = (int)(random(0, 2));
      int colChange = 1-rowChange;


      if(currentRow==4){
        colChange = 1;
        rowChange = 0;          //do south and east
      }
      else if(currentCol==4){
        rowChange = 1;
        colChange = 0;
      }

      currentRow += rowChange;
      currentCol += colChange;

      if(rowChange==1){
        if((maze[currentRow-1][currentCol]/100)%2==0){
          maze[currentRow-1][currentCol] += 100;
        }
      }
      else{
        if((maze[currentRow][currentCol-1]/10)%2==0){
          maze[currentRow][currentCol-1] += 10;
        }
      }
      numMoves++;

    }

    //Start of Draw maze

    int v [5][6]; // vertical walls
    int v1 [5][6]; // vertical walls 1
    int v2 [5][6]; // vertical walls 2

    int h [6][5]; // horizontal walls
    int h1 [6][5]; // horizontal walls 1 
    int h2 [6][5]; // horizontal walls 2

    for(int r = 0; r < 5; r++){
      for(int c = 0; c < 5; c++){
        if((r+c)%2==0){
          v1[r][c]=(maze[r][c]%2);
          v1[r][c+1] = ((maze[r][c]/10)%2);
        }
        else{
          v2[r][c]= (maze[r][c]%2);
          v2[r][c+1] = ((maze[r][c]/10)%2);
        }
      }
    }
    for(int r = 0; r < 5; r++){
      for(int c = 0; c < 5; c++){
        if((r+c)%2==0){
          h1[r][c]=((maze[r][c]/1000)%2);
          h1[r+1][c] = ((maze[r][c]/100)%2);
        }
        else{
          h2[r][c]= ((maze[r][c]/1000)%2);
          h2[r+1][c] =((maze[r][c]/100)%2);
        }
      }
    }
    for(int r = 0; r < 5; r++){
      for(int c = 0; c < 6; c++){
        if((v1[r][c]!=0)&&(v1[r][c]!=1)){
          v1[r][c]=0;
        }
        if((v2[r][c]!=0)&&(v2[r][c]!=1)){
          v2[r][c]=0;
        }
      }
    }
    for(int r = 0; r < 6; r++){
      for(int c = 0; c < 5; c++){
        if((h1[r][c]!=0)&&(h1[r][c]!=1)){
          h1[r][c]=0;
        }
        if((h2[r][c]!=0)&&(h2[r][c]!=1)){
          h2[r][c]=0;
        }
      }
    }
    
    for (int i = 0; i < 6; i++){
      for(int j = 0; j < 5; j++){
        if(h1[i][j] == 0){
          hor1[i][j] = RED;
        }
        else {
          hor1[i][j] = GREEN;
        }
        if(h2[i][j] == 0){
          hor2[i][j] = RED;
        }
        else {
          hor2[i][j] = GREEN;
        }
      }
    }

    for (int i = 0; i < 5; i++){
      for(int j = 0; j < 6; j++){
        if(v1[i][j] == 0){
          vert1[i][j] = RED;
        }
        else {
          vert1[i][j] = GREEN;
        }
        if(v2[i][j] == 0){
          vert2[i][j] = RED;
        }
        else {
          vert2[i][j] = GREEN;
        }
      }
    }

}



