enum State{
  maze1,
  maze2,
};

boolean startIsntPressed = true;
int currentButton = 3;

boolean checkIfStartIsPressed(){
  if(digitalRead(3)==1){return true;}
  else{return false;}
}

void setup(){
  pinMode(9, OUTPUT);
  pinMode(12, OUTPUT);
  pinMode(11, OUTPUT);
  pinMode(10, OUTPUT);
  pinMode(8, OUTPUT);
  pinMode(13, OUTPUT);
  pinMode(3, INPUT_PULLUP);
  pinMode(4, INPUT_PULLUP);
  mazeGenerator();
  while(startIsntPressed){
    startIsntPressed = checkIfStartIsPressed();
  }
  maze1On();    
}

boolean buttonPressed;
State currentState = maze1;

void loop(){
  while(currentButtonIsHeld(currentButton)){
    // Do Nothing
  }
  currentButton = checkAllBut(currentButton);
  currentState = switchMazes(currentState);  
}

boolean checkButtonPressed(){
  if(digitalRead(3)==1){
    return false;
  }
  else{ 
    return true;
  }
}

State switchMazes(State currentState){
  switch (currentState){
    case maze1:
    currentState = maze2;
    maze1Off();
    maze2On();
    break;

    case maze2:
    currentState = maze1;
    maze2Off();
    maze1On();
    break;
  }

  return currentState;
}


void maze1On(){
  digitalWrite(8, HIGH);
  digitalWrite(11, HIGH);
}

void maze1Off(){  
  digitalWrite(8, LOW);
  digitalWrite(11, LOW);
}

void maze2On(){
  digitalWrite(13, HIGH);  
  digitalWrite(11, HIGH);  
  digitalWrite(10, HIGH);  
  digitalWrite(9, HIGH);  
}

void maze2Off(){
  digitalWrite(13, LOW);  
  digitalWrite(11, LOW);  
  digitalWrite(10, LOW);  
  digitalWrite(9, LOW);  
}

boolean currentButtonIsHeld(int button){
  if(digitalRead(button) == 1){
    return false;
  }
   else {
    return true;
   }
  
}

void mazeGenerator(){
  
}

int checkAllBut(int lastButton){
  boolean buttonPressed = false;
   while(!buttonPressed){
     for ( int i = 3; i <= 4; i++){
        if(i != lastButton){
         if(digitalRead(i)==0){
          buttonPressed = true;
          return i;
         }
        }
     }
  }
}






