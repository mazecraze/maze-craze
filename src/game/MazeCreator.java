package game;

import java.awt.Color;

import sedgewick.StdDraw;
public class MazeCreator {

	public static void main(String [] args){
		// [rows][cols]

		int gamemode = 1;
		StdDraw.filledSquare(.5, .5, .5);
		StdDraw.setPenColor(StdDraw.RED);
		StdDraw.square(.5, .5, .5);

		double[][] v = new double[5][4]; // vertical walls
		double[][] v1 = new double[5][4]; // vertical walls 1
		double[][] v2 = new double[5][4]; // vertical walls 2

		double[][] h = new double[4][5]; // horizontal walls
		double[][] h1 = new double[4][5]; // horizontal walls 1 
		double[][] h2 = new double[4][5]; // horizontal walls 2

		if(gamemode==1){

			for(int r = 0; r < 4; r++){
				for(int c = 0; c < 5; c++){
					h1[r][c] = (Math.random());
				}
			}
			h1[3][4]=1;
			for(int r = 0; r < 4; r++){
				for(int c = 0; c < 5; c++){
					h2[r][c] = (Math.random());
				}
			}

			//vertical

			for(int r = 0; r < 5; r++){
				for(int c = 0; c < 4; c++){
					v1[r][c] = (Math.random());	
				}
			}
			v1[0][0]=1;
			for(int r = 0; r < 5; r++){
				for(int c = 0; c < 4; c++){
					v2[r][c] = (Math.random());	
				}
			}
		}
		else if(gamemode == 2){
			//horizontal
			for(int r = 0; r < 4; r++){
				for(int c = 0; c < 5; c++){
					h1[r][c] = (Math.random());
				}
			}
			
			for(int r = 0; r < 4; r++){
				for(int c = 0; c < 5; c++){
					if(h1[r][c] < 0.4)
						h2[r][c] = 1;
					else
						h2[r][c] = 0;
				}
			}
			
			//vertical
			for(int r = 0; r < 5; r++){
				for(int c = 0; c < 4; c++){
					v1[r][c] = (Math.random());	
				}
			}
			for(int r = 0; r < 5; r++){
				for(int c = 0; c < 4; c++){
					if(v1[r][c] < 0.4)
						v2[r][c] = 1;
					else
						v2[r][c] = 0;	
				}
			}
			
		}
		h=h1;
		v=v1;

		while(true){
			//draw horizontal

			for(int r = 0; r < 4; r++){
				for(int c = 0; c < 5; c++){
					if(h[r][c]<.4){
						StdDraw.setPenColor(StdDraw.RED);
						StdDraw.line(.2*c, .8-.2*r, .2*c+.2, .8-.2*r);
					}
					else{
						StdDraw.setPenColor(StdDraw.GREEN);
						StdDraw.line(.2*c, .8-.2*r, .2*c+.2, .8-.2*r);
					}
				}
			}
			//draw vertical

			for(int r = 0; r < 5; r++){
				for(int c = 0; c < 4; c++){
					if(v[r][c]<.4){
						StdDraw.setPenColor(StdDraw.RED);
						StdDraw.line(.8-.2*c, .2*r, .8-.2*c, .2*r+.2);
					}
					else{
						StdDraw.setPenColor(StdDraw.GREEN);
						StdDraw.line(.8-.2*c, .2*r, .8-.2*c, .2*r+.2);
					}
				}
			}

			if(StdDraw.mousePressed()){
				if(h==h1&&v==v1){
					h=h2;
					v=v2;
				}
				else{
					h=h1;
					v=v1;
				}
				StdDraw.show(400);
			}

		}
	}
}
