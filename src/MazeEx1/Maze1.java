package MazeEx1;
import sedgewick.StdDraw;
public class Maze1 {
	public static void main(String[] args) {
		//3rd dimension key: [N][S][E][W]

		int maze[][] = new int[5][5];
		for(int r = 0; r < 5; r++){
			for(int c = 0; c < 5; c++){
				int n = (int)(2*Math.random());
				int s = (int)(2*Math.random());
				int e = (int)(2*Math.random());
				int w = (int)(2*Math.random());
				maze[r][c] = (1000*n)+(100*s)+(10*e)+w;
			}
		}
		for(int i = 0; i < 5; i++){
			if(maze[0][i]>=1000)
				maze[0][i] = maze[0][i]-1000;
		}
		for(int i = 0; i < 5; i++){
			if((maze[4][i]/100)%2==1){
				maze[4][i] = maze[4][i]-100;
			}
		}
		for(int i = 0; i < 5; i++){
			if(maze[i][0]%2==1){
				maze[i][0] = maze[i][0]-1;
			}
		}
		for(int i = 0; i < 5; i++){
			if((maze[i][4]/10)%2==1){
				maze[i][4] = maze[i][4]-10;
			}
		}

		//Solution Implementer
		int startRow = 0;	//actually the ending point
		int startCol = 0;
		int currentRow = startRow;
		int currentCol = startCol;
		int numMoves = 0;
		while((currentRow!=4)||(currentCol!=4)){	
			int rowChange = (int)(2*Math.random());
			int colChange = 1-rowChange;


			if(currentRow==4){
				colChange = 1;
				rowChange = 0;					//do south and east
			}
			else if(currentCol==4){
				rowChange = 1;
				colChange = 0;
			}

			currentRow += rowChange;
			currentCol += colChange;

			if(rowChange==1){
				if((maze[currentRow-1][currentCol]/100)%2==0){
					maze[currentRow-1][currentCol] += 100;
				}
			}
			else{
				if((maze[currentRow][currentCol-1]/10)%2==0){
					maze[currentRow][currentCol-1] += 10;
				}
			}
			numMoves++;

		}

		System.out.println("Goal Moves: " + numMoves);

		//Start of Draw maze

		int[][] v = new int[5][6]; // vertical walls
		int[][] v1 = new int[5][6]; // vertical walls 1
		int[][] v2 = new int[5][6]; // vertical walls 2

		int[][] h = new int[6][5]; // horizontal walls
		int[][] h1 = new int[6][5]; // horizontal walls 1 
		int[][] h2 = new int[6][5]; // horizontal walls 2

		for(int r = 0; r < 5; r++){
			for(int c = 0; c < 5; c++){
				if((r+c)%2==0){
					v1[r][c]=(maze[r][c]%2);
					v1[r][c+1] = ((maze[r][c]/10)%2);
				}
				else{
					v2[r][c]= (maze[r][c]%2);
					v2[r][c+1] = ((maze[r][c]/10)%2);
				}
			}
		}
		for(int r = 0; r < 5; r++){
			for(int c = 0; c < 5; c++){
				if((r+c)%2==0){
					h1[r][c]=((maze[r][c]/1000)%2);
					h1[r+1][c] = ((maze[r][c]/100)%2);
				}
				else{
					h2[r][c]= ((maze[r][c]/1000)%2);
					h2[r+1][c] =((maze[r][c]/100)%2);
				}
			}
		}
		for(int r = 0; r < 5; r++){
			for(int c = 0; c < 6; c++){
				if((v1[r][c]!=0)&&(v1[r][c]!=1)){
					v1[r][c]=0;
				}
				if((v2[r][c]!=0)&&(v2[r][c]!=1)){
					v2[r][c]=0;
				}
			}
		}
		for(int r = 0; r < 6; r++){
			for(int c = 0; c < 5; c++){
				if((h1[r][c]!=0)&&(h1[r][c]!=1)){
					h1[r][c]=0;
				}
				if((h2[r][c]!=0)&&(h2[r][c]!=1)){
					h2[r][c]=0;
				}
			}
		}

		StdDraw.setXscale(0, 1);
		StdDraw.setYscale(1, 0);
		StdDraw.filledSquare(.5, .5, .5);
		h=h1;
		v=v1;
		boolean gameOn = true;
		while(gameOn){

			//draw horizontal

			for(int r = 0; r < 6; r++){
				for(int c = 0; c < 5; c++){
					if(h[r][c]!=1){
						StdDraw.setPenColor(StdDraw.RED);
						StdDraw.line((1/6.0)*c, (1/6.0)*r, (1/6.0)*c+(1/6.0), (1/6.0)*r);
					}
					else{
						StdDraw.setPenColor(StdDraw.GREEN);
						StdDraw.line((1/6.0)*c, (1/6.0)*r, (1/6.0)*c+(1/6.0), (1/6.0)*r);
					}
				}
			}
			//draw vertical

			for(int r = 0; r < 5; r++){
				for(int c = 0; c < 6; c++){
					if(v[r][c]!=1){
						StdDraw.setPenColor(StdDraw.RED);
						StdDraw.line((1/6.0)*c, (1/6.0)*r, (1/6.0)*c, (1/6.0)*r+(1/6.0));
					}
					else{
						StdDraw.setPenColor(StdDraw.GREEN);
						StdDraw.line((1/6.0)*c, (1/6.0)*r, (1/6.0)*c, (1/6.0)*r+(1/6.0));
					}
				}
			}

			if(StdDraw.mousePressed()){
				if(h==h1&&v==v1){
					h=h2;
					v=v2;
				}
				else{
					h=h1;
					v=v1;
				}
				StdDraw.show(400);
			}
		}
	}
}
