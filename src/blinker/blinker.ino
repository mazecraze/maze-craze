#include <Adafruit_NeoPixel.h>
#include "WS2812_Definitions.h"

Adafruit_NeoPixel leds2 = Adafruit_NeoPixel(15, 2, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel leds3 = Adafruit_NeoPixel(15, 3, NEO_GRB + NEO_KHZ800);


void setup(){
  leds2.begin();
  leds3.begin();  
}
int count = 0;

boolean on = false;

void loop (){
  if(on){
    clearLEDs();
    on = false;
  }
  else {
    for(int i = 0; i < 15; i++){
      leds2.setPixelColor(i, count, 8*i, 128/i);
      leds3.setPixelColor(i, count, 128/i, 8*i);
    }
    on = true;
  }
    
  leds2.show();
  leds3.show();
  delay(500);
  if(count<128){count += 5;}
}


void clearLEDs(){
  for (int i = 0; i<15; i++){
    leds2.setPixelColor(i, 0);
    leds3.setPixelColor(i, 0);
  }
}

